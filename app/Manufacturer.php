<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $model
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Vehicle[] $vehicles
 */
class Manufacturer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'model'];

    /**
     * The on-to-many relationsip between the Manufacturer and Vehicle.
     * Returns the vehicles for this manufacturer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Vehicle[]
     */
    public function vehicles()
    {
        return $this->hasMany('App\Vehicle');
    }
}
