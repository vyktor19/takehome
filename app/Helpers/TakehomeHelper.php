<?php

namespace App\Helpers;

use App\Manufacturer;
use App\Owner;
use App\Vehicle;
use Exception;
use SimpleXMLElement;
use Symfony\Component\Console\Helper\Helper;

/**
 * Helper for reusable functionality.
 */
class TakehomeHelper extends Helper
{

    /**
     * Returns the canonical name of this helper.
     *
     * @return string The canonical name
     */
    public function getName()
    {
        return 'takehome';
    }

    /**
     * Import a Vehicles XML file into the database.
     *
     * @param string $filename The XML file to import.
     *
     * @return string|bool
     *   String with errors if any were found,
     *   TRUE if import was successful.
     */
    public static function importVehiclesXML($filename)
    {
        try {
            $xml_string = file_get_contents($filename);

            if (!$xml_string)
            {
                throw new Exception('Could not read from the file.');
            }
            elseif (empty($xml_string))
            {
                throw new Exception('File is empty.');
            }

            // Check if the XML is a valid XML.
            libxml_use_internal_errors(true);
            // Load the XML into memory.
            $xml_object = simplexml_load_string($xml_string);

            $errors = libxml_get_errors();
            if (!empty($errors))
            {
                // If there were errors, concatenate them and throw them as Exception.
                throw new Exception(implode(', ', $errors));
            }
            libxml_use_internal_errors();

            foreach ($xml_object->children() as $vehicle)
            {
                // Check if the Manufacturer exists, if not create a new one.
                /** @var \App\Manufacturer $manufacturer */
                $manufacturer = Manufacturer::firstOrNew(['name' => $vehicle['manufacturer'], 'model' => $vehicle['model']]);
                if (!$manufacturer->id)
                {
                    // Save the new manufacturer, if needed.
                    $manufacturer->save();
                }

                // Check if the Owner exists, if not create a new one.
                /** @var \App\Owner $owner */
                $owner = Owner::firstOrNew(['name' => $vehicle->owner_name]);
                if (!$owner->id)
                {
                    $owner->name = $vehicle->owner_name;
                    $owner->company = $vehicle->owner_company;
                    $owner->profession = $vehicle->owner_profession;

                    // Save the new owner, if needed.
                    $owner->save();
                }

                // Create the new Vehicle object, but only if it is not already there.
                // Edit in case it already exists; the licence_place should unique.
                /** @var \App\Vehicle $vehicle_obj */
                $vehicle_obj = Vehicle::firstOrNew(['licence_plate' => $vehicle->license_plate]);

                $vehicle_obj->type = $vehicle->type;
                $vehicle_obj->usage = $vehicle->usage;
                $vehicle_obj->licence_plate = $vehicle->license_plate;
                $vehicle_obj->weight_category = $vehicle->weight_category;
                $vehicle_obj->seats =  $vehicle->no_seats;
                $vehicle_obj->boot = self::getBoolValue($vehicle->has_boot);
                $vehicle_obj->trailer = self::getBoolValue($vehicle->has_trailer);
                $vehicle_obj->transmission = $vehicle->transmission;
                $vehicle_obj->colour = $vehicle->colour;
                $vehicle_obj->hgv = self::getBoolValue($vehicle->is_hgv);
                $vehicle_obj->doors = $vehicle->no_doors;
                $vehicle_obj->sunroof = self::getBoolValue($vehicle->sunroof);
                $vehicle_obj->gps = self::getBoolValue($vehicle->has_gps);
                $vehicle_obj->wheels = $vehicle->no_wheels;
                $vehicle_obj->engine_cc = $vehicle->engine_cc;
                $vehicle_obj->fuel = $vehicle->fuel_type;

                // Set the manufacturer and owner.
                $vehicle_obj->manufacturer()->associate($manufacturer);
                $vehicle_obj->owner()->associate($owner);

                // Save the new Vehicle into the database.
                $vehicle_obj->save();
            }
        } catch (Exception $ex)
        {
            // Or we could let the caller handle it. For the moment we will return only the messages.
            return $ex->getMessage();
        }

        return true;
    }

    /**
     * Transforms the value of a variable into bool.
     *
     * Values accepted: bool, string, null, int.
     * If the value is bool, the value itself is returned.
     * If the value is numeric, 0 is false and all else are true.
     * If the value is string, 'true' is true, all else are false. The value can be upper, lower or any combination
     *   of cases.
     * If the value is an SimpleXMLElement, the value is transformed into string and checked again.
     * All other types and values return false.
     *
     * @example 'true' => true
     * @example 1 => true
     * @example 0 => false
     * @example true => true
     * @example null => false
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function getBoolValue($value)
    {
        if (is_bool($value))
        {
            // The value is already boolean.
            return $value;
        }
        elseif (is_numeric($value) && $value != 0)
        {
            // The value is numeric, so 0 is false, all others are true.
            return true;
        }
        elseif (is_string($value) && mb_strtoupper(trim($value)) === 'TRUE')
        {
            // The value is a string, so we accept string 'true' as true, all else are false.
            return true;
        }
        elseif ($value instanceof SimpleXMLElement)
        {
            return self::getBoolValue((string) $value);
        }

        return false;
    }
}
