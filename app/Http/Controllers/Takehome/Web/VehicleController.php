<?php

namespace App\Http\Controllers\Takehome\Web;

use App\Http\Controllers\Controller;
use App\Vehicle;

/**
 * Controls the Web part of the Vehicle.
 */
class VehicleController extends Controller
{

    /**
     * Display all the vehicles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /** @var \App\Vehicle[] $vehicles */
        $vehicles = Vehicle::with(['manufacturer', 'owner'])->get();
        return view('takehome.vehicle.vehicles', ['vehicles' => $vehicles]);
    }
}
