<?php

namespace App\Http\Controllers\Takehome\Web;

use App\Http\Controllers\Controller;
use App\Manufacturer;

/**
 * Controls the Web part of the Manufacturer.
 */
class ManufacturerController extends Controller
{

    /**
     * Display all the manufacturers.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('takehome.manufacturer.manufacturers', ['manufacturers' => Manufacturer::all()]);
    }
}
