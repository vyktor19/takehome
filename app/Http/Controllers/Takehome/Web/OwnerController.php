<?php

namespace App\Http\Controllers\Takehome\Web;

use App\Http\Controllers\Controller;
use App\Owner;

/**
 * Controls the Web part of the Owner.
 */
class OwnerController extends Controller
{

    /**
     * Display all the owners.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('takehome.owners.owners', ['owners' => Owner::all()]);
    }
}
