<?php

namespace App\Http\Controllers\Takehome\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\VehicleResource;
use App\Http\Resources\VehiclesResource;
use App\Vehicle;

/**
 * Controls the API part of the Vehicle.
 */
class VehicleController extends Controller
{

    /**
     * Display all the vehicles.
     *
     * @return \App\Http\Resources\VehiclesResource
     */
    public function index()
    {
        /** @var \App\Vehicle[] $vehicles */
        $vehicles = Vehicle::with(['manufacturer', 'owner'])->get();
        return new VehiclesResource($vehicles);
    }

    /**
     * Display the specified vehicle.
     *
     * @param \App\Vehicle $vehicle
     *
     * @return \App\Http\Resources\VehicleResource
     */
    public function show(Vehicle $vehicle)
    {
        VehicleResource::withoutWrapping();

        return new VehicleResource($vehicle);
    }
}
