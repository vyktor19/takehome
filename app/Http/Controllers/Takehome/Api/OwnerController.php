<?php

namespace App\Http\Controllers\Takehome\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OwnerResource;
use App\Http\Resources\OwnersResource;
use App\Owner;

/**
 * Controls the API part of the Owner.
 */
class OwnerController extends Controller
{

    /**
     * Display all the owners.
     *
     * @return \App\Http\Resources\OwnersResource
     */
    public function index()
    {
        return new OwnersResource(Owner::all());
    }

    /**
     * Display the specified owner.
     *
     * @param \App\Owner $owner
     *
     * @return \App\Http\Resources\OwnerResource
     */
    public function show(Owner $owner)
    {
        OwnerResource::withoutWrapping();

        return new OwnerResource($owner);
    }
}
