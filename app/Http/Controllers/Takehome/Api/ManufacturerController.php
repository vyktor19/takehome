<?php

namespace App\Http\Controllers\Takehome\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ManufacturerResource;
use App\Http\Resources\ManufacturersResource;
use App\Manufacturer;

/**
 * Controls the API part of the Manufacturer.
 */
class ManufacturerController extends Controller
{

    /**
     * Display a listing of all the manufacturers.
     *
     * @return \App\Http\Resources\ManufacturersResource
     */
    public function index()
    {
        return new ManufacturersResource(Manufacturer::all());
    }

    /**
     * Display the specified manufacturer.
     *
     * @param \App\Manufacturer $manufacturer
     *
     * @return \App\Http\Resources\ManufacturerResource
     */
    public function show(Manufacturer $manufacturer)
    {
        ManufacturerResource::withoutWrapping();

        return new ManufacturerResource($manufacturer);
    }
}
