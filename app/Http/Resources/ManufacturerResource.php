<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ManufacturerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'manufacturers',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'model' => $this->model,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ],
            'relationships' => [
                'vehicles' => VehicleIdentifierResource::collection($this->vehicles)
            ],
        ];
    }
}
