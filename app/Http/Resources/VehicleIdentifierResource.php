<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Used in relationships to only give basic information on the Model and prevent recursive data,
 * leading to infinite loops.
 */
class VehicleIdentifierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'vehicles',
            'id' => $this->id,
        ];
    }
}
