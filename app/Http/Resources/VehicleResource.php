<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehicleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'vehicles',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'type' => $this->type,
                'usage' => $this->usage,
                'licence_plate' => $this->licence_plate,
                'weight_category' => $this->weight_category,
                'no_seats' => $this->seats,
                'has_boot' => (bool) $this->boot,
                'has_trailer' => (bool) $this->trailer,
                'transmission' => $this->transmission,
                'colour' => $this->colour,
                'is_hgv' => (bool) $this->hgv,
                'no_doors' => $this->doors,
                'sunroof' => (bool) $this->sunroof,
                'has_gps' => (bool) $this->gps,
                'no_wheels' => $this->wheels,
                'engine_cc' => $this->engine_cc,
                'fuel_type' => $this->fuel,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ],
            'relationships' => [
                'manufacturers' => new ManufacturerIdentifierResource($this->manufacturer),
                'owners' => new OwnerIdentifierResource($this->owner),
            ],
        ];
    }
}
