<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Vehicle model.
 *
 * @property int $id
 * @property string $type
 * @property string $usage
 * @property string $licence_plate
 * @property integer $weight_category
 * @property integer $seats
 * @property boolean $boot
 * @property boolean $trailer
 * @property string $transmission
 * @property string $colour
 * @property boolean $hgv
 * @property integer $doors
 * @property boolean $sunroof
 * @property boolean $gps
 * @property integer $wheels
 * @property integer $engine_cc
 * @property string $fuel
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Manufacturer $manufacturer
 * @property Owner $owner
 */
class Vehicle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['licence_plate'];

    /**
     * Revered on-to-many relationship for Manufacturer.
     * Returns the Manufacturer of this Vehicle.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Manufacturer
     */
    public function manufacturer()
    {
        return $this->belongsTo('App\Manufacturer');
    }

    /**
     * Revered on-to-many relationship for Owner.
     * Returns the Manufacturer of this Vehicle.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Manufacturer
     */
    public function owner()
    {
        return $this->belongsTo('App\Owner');
    }
}
