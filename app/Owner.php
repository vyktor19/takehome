<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $company
 * @property string $profession
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Vehicle[] $vehicles
 */
class Owner extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The on-to-many relationship between the Owner and Vehicle.
     * Returns the vehicles for this manufacturer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Vehicle[]
     */
    public function vehicles()
    {
        return $this->hasMany('App\Vehicle');
    }
}
