<?php

namespace Tests\Feature\Api;

use App\User;
use Tests\TestCase;

/**
 * Test the logout functionality of the API.
 */
class LogoutTest extends TestCase
{
    /**
     * Test the logout route for the API actually logs the user out.
     */
    public function testUserIsLoggedOutProperly()
    {
        $user = factory(User::class)->create(['email' => 'user@test.com']);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $this->json('get', '/api/owners', [], $headers)->assertStatus(200);
        $this->json('post', '/api/logout', [], $headers)->assertStatus(200);

        $user = User::find($user->id);

        $this->assertEquals(null, $user->api_token);
    }

    /**
     * Test the user cannot access the API after logout, even if the token was valid at some point.
     */
    public function testUserWithNullToken()
    {
        // Simulating login.
        $user = factory(User::class)->create(['email' => 'user@test.com']);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        // Simulating logout.
        $user->api_token = null;
        $user->save();

        $this->json('get', '/api/owners', [], $headers)->assertStatus(401);
    }
}
