<?php

namespace Tests\Feature\Api;

use App\User;
use Tests\TestCase;

/**
 * Test the manufacturers API.
 */
class ManufacturersTest extends TestCase
{

    /**
     * Test the manufacturers list is retrieved from the API, with login.
     */
    public function testManufacturersAreListedCorrectly()
    {
        /** @var \App\User $user */
        $user = User::find(1);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/manufacturers', [], $headers);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'type',
                        'id',
                        'attributes' => [
                            'name',
                            'model',
                            'created_at',
                            'updated_at',
                        ],
                        'relationships' => ['vehicles'],
                    ],
                ],
            ]);
    }

    /**
     * Test the single manufacturer is retrieved correctly from the API, with login.
     */
    public function testManufacturerIsRetrievedCorrectly()
    {
        /** @var \App\User $user */
        $user = User::find(1);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/manufacturers/1', [], $headers);

        // Did not test the data itself because of the created and updated dates/
        $response->assertStatus(200)
            ->assertJsonStructure([
                'type',
                'id',
                'attributes' => [
                    'name',
                    'model',
                    'created_at',
                    'updated_at',
                ],
                'relationships' => ['vehicles'],
            ]);
    }
}
