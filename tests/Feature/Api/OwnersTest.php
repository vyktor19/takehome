<?php

namespace Tests\Feature\Api;

use App\User;
use Tests\TestCase;

/**
 * Test the owners API.
 */
class OwnersTest extends TestCase
{

    /**
     * Test the owners list is retrieved from the API, with login.
     */
    public function tesOwnersAreListedCorrectly()
    {
        /** @var \App\User $user */
        $user = User::find(1);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/owners', [], $headers);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'type',
                        'id',
                        'attributes' => [
                            'name',
                            'company',
                            'profession',
                            'created_at',
                            'updated_at',
                        ],
                        'relationships' => ['vehicles'],
                    ],
                ],
            ]);
    }

    /**
     * Test the single owners is retrieved correctly from the API, with login.
     */
    public function testOwnerIsRetrievedCorrectly()
    {
        /** @var \App\User $user */
        $user = User::find(1);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/owners/1', [], $headers);
        $json = $response->json();

        // Did not test the data itself because of the created and updated dates/
        $response->assertStatus(200)
            ->assertJsonStructure([
                'type',
                'id',
                'attributes' => [
                    'name',
                    'company',
                    'profession',
                    'created_at',
                    'updated_at',
                ],
                'relationships' => ['vehicles'],
            ]);
    }
}
