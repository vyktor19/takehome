<?php

namespace Tests\Feature\Api;

use App\User;
use Tests\TestCase;

/**
 * Test the vehicles API.
 */
class VehiclesTest extends TestCase
{

    /**
     * Test the vehicles list is retrieved from the API, with login.
     */
    public function testVehiclesAreListedCorrectly()
    {
        /** @var \App\User $user */
        $user = User::find(1);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/vehicles', [], $headers);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'type',
                        'id',
                        'attributes' => [
                            'name',
                            'type',
                            'usage',
                            'licence_plate',
                            'weight_category',
                            'no_seats',
                            'has_boot',
                            'has_trailer',
                            'transmission',
                            'colour',
                            'is_hgv',
                            'no_doors',
                            'sunroof',
                            'has_gps',
                            'no_wheels',
                            'engine_cc',
                            'fuel_type',
                            'created_at',
                            'updated_at',
                        ],
                        'relationships' => [
                            'manufacturers',
                            'owners',
                        ],
                    ],
                ],
            ]);
    }

    /**
     * Test the single vehicle is retrieved correctly from the API, with login.
     */
    public function testVehicleIsRetrievedCorrectly()
    {
        /** @var \App\User $user */
        $user = User::find(1);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/vehicles/1', [], $headers);

        // Did not test the data itself because of the created and updated dates/
        $response->assertStatus(200)
            ->assertJsonStructure([
                'type',
                'id',
                'attributes' => [
                    'name',
                    'type',
                    'usage',
                    'licence_plate',
                    'weight_category',
                    'no_seats',
                    'has_boot',
                    'has_trailer',
                    'transmission',
                    'colour',
                    'is_hgv',
                    'no_doors',
                    'sunroof',
                    'has_gps',
                    'no_wheels',
                    'engine_cc',
                    'fuel_type',
                    'created_at',
                    'updated_at',
                ],
                'relationships' => [
                    'manufacturers',
                    'owners',
                ],
            ]);
    }
}
