<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Create the table for the Vehicle model.
 */
class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manufacturer_id')->unsigned();
            $table->integer('owner_id')->unsigned();
            $table->string('type');
            $table->string('usage');
            $table->string('licence_plate');
            $table->integer('weight_category');
            $table->integer('seats');
            $table->boolean('boot');
            $table->boolean('trailer');
            $table->string('transmission');
            $table->string('colour');
            $table->boolean('hgv');
            $table->integer('doors');
            $table->boolean('sunroof');
            $table->boolean('gps');
            $table->integer('wheels');
            $table->integer('engine_cc');
            $table->string('fuel');
            $table->timestamps();

            $table->foreign('manufacturer_id')
                ->references('id')->on('manufacturers')
                ->onDelete('cascade');
            $table->foreign('owner_id')
                ->references('id')->on('owners')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
