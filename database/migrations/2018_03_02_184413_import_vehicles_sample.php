<?php

use App\Helpers\TakehomeHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Import the Sample file.
 */
class ImportVehiclesSample extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Import the file with the sample vehicles.
        $status = TakehomeHelper::importVehiclesXML(__DIR__ . DIRECTORY_SEPARATOR .  'VehicleSample.xml');

        if (!$status)
        {
            printf('Something went wrong while migrating ' . __FILE__);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Vehicle::query()->truncate();
        \App\Manufacturer::query()->truncate();
        \App\Owner::query()->truncate();
    }
}
