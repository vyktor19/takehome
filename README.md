A vehicle database system.

## General

This is a site that holds and displays a vehicle collection.

The core Laravel functionality is used.

## Functionality provided
* Creates the database and import a file with sample vehicles.
* Displays the vehicles on the homepage.
* Login to hide owners page (sensitive information).
* Exposed data through RESTful API.
* Created tests to ensure the API works, as they can also be used as examples to create a client in Laravel.

## Installation
* Run `composer install`
* Create `.env` config file from the`.env.example` file and fill the  database parameters with your credentials
  * Run `php artisan migrate` to update your database schema and import the data.
  * Login: `admin@mail.com` / `admin` combination

## Api examples:
### Login:
Login using email/password combination to get an authorization token.

`curl -X POST localhost/api/login
   -H "Accept: application/json"
   -H "Content-type: application/json"
   -d "{"email": "admin@mail.com", "password": "admin"}"`
   
### Retrieve information
To retrieve information, first login to receive the authorization token.

`curl -X POST localhost/api/owners
   -H "Accept: application/json"
   -H "Content-type: application/json"
   -H "Authorization: Bearer $token"`

### Logout
Clears the user's authorization token.

`curl -X POST localhost/api/logout
   -H "Accept: application/json"
   -H "Content-type: application/json"
   -H "Authorization: Bearer $token"`
