<!doctype html>
<html lang="en">
    <head>
        <title>Takehome - @yield('title')</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/blog.min.css') }}" rel="stylesheet">
        @stack('styles')
    </head>
    <body>
        <header>
            <div class="blog-header mt-5">
                <div class="container">
                    <h1 class="blog-title">@yield('header')</h1>
                </div>
            </div>
        </header>
        <div class="blog-masthead">

            <div class="container">
                <nav class="nav">
                    <a class="nav-link {{ (url()->current() == route('home')) ? 'active' : ''}}" href="{{ route('home') }}">Vehicles</a>
                    <a class="nav-link {{ (url()->current() == route('manufacturers')) ? 'active' : ''}}" href="{{ route('manufacturers') }}">Manufacturers</a>
                    @if(Auth::check())
                        {{--The owners contain sensitive information, so only logged in uses should be able to access it.--}}
                        <a class="nav-link {{ (url()->current() == route('owners')) ? 'active' : ''}}" href="{{ route('owners') }}">Owners</a>
                        <a class="nav-link {{ (url()->current() == route('logout_form')) ? 'active' : ''}}" href="{{ route('logout_form') }}">Logout</a>
                    @endif
                    @if(Auth::guest())
                        <a class="nav-link {{ (url()->current() == route('login')) ? 'active' : ''}}" href="{{ route('login') }}">Login</a>
                    @endif
                </nav>
            </div>
        </div>
        <main role="main" class="container">
            <div class="container">
                @yield('content')
            </div>
        </main>
    </body>
</html>
