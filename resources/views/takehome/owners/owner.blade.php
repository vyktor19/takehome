<div class="card mb-3">
    <div class="card-body">
        <h5 class="card-title">{{ $owner->name }}</h5>
        <p class="card-title">Company: {{ $owner->company }}</p>
        <p class="card-title">Profession: {{ $owner->profession }}</p>
    </div>
</div>
