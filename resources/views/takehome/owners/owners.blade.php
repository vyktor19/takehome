@extends('layouts.takehome')

@section('title', 'Owners')

@section('header', 'Owners')

@section('content')
    @foreach ($owners as $owner)
        @include('takehome.owners.owner', $owner)
    @endforeach
@endsection
