@extends('layouts.takehome')

@section('title', 'Vehicles')

@section('header', 'Vehicles')

@section('content')
    @foreach ($vehicles as $vehicle)
        @include('takehome.vehicle.vehicle', $vehicle)
    @endforeach
@endsection
