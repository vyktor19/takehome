<div class="card mb-3">
    <div class="card-body">
        <h5 class="card-title">{{ $vehicle->manufacturer->name }} {{ $vehicle->manufacturer->model }}</h5>
        <h6 class="card-subtitle mb-2 text-muted">Owner: {{ $vehicle->owner->name }}</h6>
        <table class="table">
            <tbody>
            <tr>
                <td>Type: {{ $vehicle->type }}</td>
                <td>Licence plate: {{ $vehicle->licence_plate }}</td>
                <td>Weight Category: {{ $vehicle->weight_category }}</td>
            </tr>
            <tr>
                <td>Nr. of seats: {{ $vehicle->seats }}</td>
                <td>Has Trailer: {{ $vehicle->seats ? 'yes' : 'no' }}</td>
                <td>Transmission: {{ $vehicle->transmission }}</td>
            </tr>
            <tr>
                <td>Colour: {{ $vehicle->colour }}</td>
                <td>Nr. of doors: {{ $vehicle->doors }}</td>
                <td>Sunroof: {{ $vehicle->sunroof ? 'yes' : 'no' }}</td>
            </tr>
            <tr>
                <td>GPS: {{ $vehicle->gps ? 'yes' : 'no' }}</td>
                <td>Nr. of wheels: {{ $vehicle->wheels }}</td>
                <td>Engine capacity: {{ $vehicle->engine_cc }}</td>
            </tr>
            <tr>
                <td colspan="3">Fuel type: {{ $vehicle->fuel }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
