@extends('layouts.takehome')

@section('title', 'Manufacturers')

@section('header', 'Manufacturers')

@section('content')
    @foreach ($manufacturers as $manufacturer)
        @include('takehome.manufacturer.manufacturer', $manufacturer)
    @endforeach
@endsection
