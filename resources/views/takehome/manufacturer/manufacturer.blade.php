<div class="card mb-3">
    <div class="card-body">
        <h5 class="card-title">{{ $manufacturer->name }} {{ $manufacturer->model }}</h5>
    </div>
</div>
