<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Takehome\Web\VehicleController@index')->name('home');
Route::get('/owners', 'Takehome\Web\OwnerController@index')->middleware('auth')->name('owners');
Route::get('/manufacturers', 'Takehome\Web\ManufacturerController@index')->name('manufacturers');
Route::get('/logout', 'Auth\LoginController@showLogoutForm')->name('logout_form');

Auth::routes();
